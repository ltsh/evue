import './index.scss';

export default {
  name: 'e-style',
  data() {
    return {
      obj: {},
      option: {
        column: [
          {
            style:'background: #679fce;',
            className:'my-name-class',
            children: [
              {
                itemLabel: '基础 :',
                itemProp: 'value',
                itemRules: [
                  {required: true, message: '请输入名称', trigger: 'blur'}
                ],
                type: 'input',
                model: 'value',
                placeholder: '请输入',
                style:'width:100px',
                className:'myClassName',
                desc:`我是最外层 div 有一个style:<span style="color:#9a1818"> background: #679fce;</span>,还有一个名字叫 <span style="color:#9a1818">my-name-class</span> 的class,里层children属性也有<span style="color:#ffaca1">style</span>跟<span style="color:#41329a"> className</span>的属性`
              },
            ]
          },
        ]
      }
    }

  },
  watch: {},
  mounted() {
  },
  methods: {},
}